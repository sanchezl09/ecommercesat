<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class VentaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ventas')->insert([
            'importeTotal'=>1232,
            'cantidad_productos'=>10,
            'fecha_venta'=>Carbon::now(),
            'estatusEliminar'=>1,
            'id_user'=>1,

            ]);
    }
}

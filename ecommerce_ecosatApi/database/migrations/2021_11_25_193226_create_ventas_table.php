<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id();
            $table->decimal('importeTotal', 18,2);
            $table->date('fecha_venta');
            $table->integer('cantidad_productos');
            $table->tinyInteger('estatusEliminar');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users');      
            $table->timestamps();
 
         });
    }







    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}

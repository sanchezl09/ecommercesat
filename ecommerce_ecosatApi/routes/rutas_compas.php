<?php 
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ComprasController;
use App\Http\Controllers\ProductosController;


//Almacenes


Route::get('compras/{id}',  [ComprasController::class, 'index']);
Route::get('compras/view/{id}',  [ComprasController::class, 'show']);
Route::get('compras',  [ComprasController::class, 'getCompras']);

Route::post('compras',  [ComprasController::class, 'store']);
Route::put('compras/{id}',  [ComprasController::class, 'update']);
Route::delete('compras/{id}',  [ComprasController::class, 'delete']);



Route::get('productos',  [ProductosController::class, 'index']);
Route::get('productos/view/{id}',  [ProductosController::class, 'show']);
Route::post('productos',  [ProductosController::class, 'store']);
Route::put('productos/{id}',  [ProductosController::class, 'update']);
Route::delete('productos/{id}',  [ProductosController::class, 'delete']);
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    use HasFactory;
    protected $table="ventas";
    protected $fillable = ['importeTotal','fecha_venta',
        'cantidad_productos','estatusEliminar','id_user',
       ];
}

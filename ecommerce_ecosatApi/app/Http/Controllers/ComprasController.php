<?php

namespace App\Http\Controllers;
use App\Models\Compra;

use Illuminate\Http\Request;

class ComprasController extends Controller
{
    public function index($id)
    {
        $compra = Compra::where('id_user',$id)->get();
        return $compra;
    }

    
   
    public function store(Request $request)
    {
        $data = Compra::create($request->all());
        return response()->json($data, 200);
    }
    public function getCompras()
    {
        $compra = Compra::all();
        return $compra;
    }
   
    public function show($id)
    {
        $compra = Compra::findOrFail($id);
        return $compra;   
     }

    
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

    
    public function delete($id)
    {
        $data = Compra::findOrFail($id);
        $data->delete();

        return response()->json([
            'success'=>true,
            'message'=>'Deleted Successfully'
        ],204);
    }
}
